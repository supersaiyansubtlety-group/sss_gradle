package net.sssubtlety.sss_gradle.util;

import static org.gradle.api.plugins.JavaPlugin.IMPLEMENTATION_CONFIGURATION_NAME;

public final class StringUtil {
    private StringUtil() { }

    public static String capitalize(String string) {
        return string.isEmpty() ? string : string.substring(0, 1).toUpperCase() + string.substring(1);
    }
}
