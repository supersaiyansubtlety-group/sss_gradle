package net.sssubtlety.sss_gradle.plugin;

import net.sssubtlety.sss_gradle.Constants;
import org.gradle.api.Plugin;
import org.gradle.api.Project;
import org.gradle.api.artifacts.VersionCatalog;
import org.gradle.api.artifacts.VersionCatalogsExtension;
import org.gradle.api.artifacts.dsl.DependencyHandler;
import org.gradle.api.plugins.JavaBasePlugin;
import org.gradle.api.plugins.JavaPluginExtension;
import org.gradle.api.tasks.SourceSetContainer;
import org.gradle.api.tasks.TaskContainer;
import org.gradle.api.tasks.testing.Test;
import org.jetbrains.annotations.NotNull;

import java.util.Optional;

import static java.lang.System.lineSeparator;
import static net.sssubtlety.sss_gradle.plugin.TestSourceSetsPlugin.*;
import static net.sssubtlety.sss_gradle.util.StringUtil.capitalize;
import static org.gradle.api.plugins.JavaPlugin.*;

@SuppressWarnings("unused")
public class SpockTestsPlugin implements Plugin<Project> {
    @Override
    public void apply(@NotNull Project project) {
        final var plugins = project.getPlugins();

        plugins.apply(TestSourceSetsPlugin.class);

        final var sssLibs = Optional.ofNullable(project.getExtensions().findByType(VersionCatalogsExtension.class))
            .flatMap(catalogs -> catalogs.find(Constants.SSS_LIBS))
            .orElseThrow(() -> new IllegalStateException(
                Constants.SSS_LIBS + " version catalog missing. Add the following to your settings.gradle file and replace " +
                "<version> with your desired version:" + lineSeparator() +
                """
                dependencyResolutionManagement {
                    repositories {
                        maven {
                            name 'sss_gradle'
                            url sss_gradle_maven_url
                        }
                    }
                    versionCatalogs {
                        sssLibs {
                            from 'net.sssubtlety:sss_version_catalog:<version>'
                        }
                    }
                }
                """
            ));

        final var dependencies = project.getDependencies();

        addTestDependencies(
            dependencies, sssLibs,
            TEST_IMPLEMENTATION_CONFIGURATION_NAME, TEST_RUNTIME_ONLY_CONFIGURATION_NAME
        );

        addTestDependencies(
            dependencies, sssLibs,
            INTEGRATION_TEST_IMPLEMENTATION, INTEGRATION_TEST + capitalize(RUNTIME_ONLY_CONFIGURATION_NAME)
        );

        addTestDependencies(
            dependencies, sssLibs,
            FUNCTIONAL_TEST_IMPLEMENTATION, FUNCTIONAL_TEST + capitalize(RUNTIME_ONLY_CONFIGURATION_NAME)
        );

        final var tasks = project.getTasks();
        final var sourceSets = project.getExtensions().getByType(JavaPluginExtension.class).getSourceSets();

        registerTestTask(tasks, sourceSets, INTEGRATION_TEST, "Runs the integration tests.");
        registerTestTask(tasks, sourceSets, FUNCTIONAL_TEST, "Runs the functional tests.", INTEGRATION_TEST);

        tasks.named(JavaBasePlugin.CHECK_TASK_NAME, check -> check.dependsOn(INTEGRATION_TEST, FUNCTIONAL_TEST));

        tasks.withType(Test.class).configureEach(Test::useJUnitPlatform);
    }

    private static void addTestDependencies(
        DependencyHandler dependencies, VersionCatalog libs,
        String implementation, String runtimeOnly
    ) {
        dependencies.add(implementation, dependencies.platform(libs.findLibrary("spock_bom").orElseThrow()));
        dependencies.add(implementation, libs.findLibrary("spock_core").orElseThrow());
        dependencies.add(runtimeOnly, libs.findLibrary("spock_core").orElseThrow());
        dependencies.add(implementation, dependencies.gradleTestKit());
    }

    private static void registerTestTask(
        TaskContainer tasks, SourceSetContainer sourceSets,
        String sourceSetAndTaskName, String description,
        String... mustRunAfterAdditions
    ) {
        tasks.register(sourceSetAndTaskName, Test.class, test -> {
            test.setDescription(description);
            test.setGroup("verification");
            final var integrationSourceSet = sourceSets.getByName(sourceSetAndTaskName);
            test.setTestClassesDirs(integrationSourceSet.getOutput().getClassesDirs());
            test.setClasspath(integrationSourceSet.getRuntimeClasspath());
            test.mustRunAfter("test");
            test.mustRunAfter(mustRunAfterAdditions);
        });
    }
}
