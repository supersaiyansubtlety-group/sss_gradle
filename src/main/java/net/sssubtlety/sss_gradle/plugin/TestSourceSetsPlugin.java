package net.sssubtlety.sss_gradle.plugin;

import net.sssubtlety.sss_gradle.Constants;
import org.gradle.api.Plugin;
import org.gradle.api.Project;
import org.gradle.api.plugins.GroovyPlugin;
import org.gradle.api.plugins.JavaPlugin;
import org.gradle.api.plugins.JavaPluginExtension;
import org.gradle.api.tasks.GroovySourceDirectorySet;
import org.jetbrains.annotations.NotNull;

import static net.sssubtlety.sss_gradle.util.StringUtil.capitalize;
import static org.gradle.api.plugins.JavaPlugin.IMPLEMENTATION_CONFIGURATION_NAME;

public class TestSourceSetsPlugin implements Plugin<Project> {
    public static final String INTEGRATION_TEST = "integrationTest";
    public static final String FUNCTIONAL_TEST = "functionalTest";

    public static final String FUNCTIONAL_TEST_IMPLEMENTATION;
    public static final String INTEGRATION_TEST_IMPLEMENTATION;

    public static final String INTEGRATION_PATH = "src/test/integration/";
    public static final String FUNCTIONAL_PATH = "src/test/functional/";
    public static final String UNIT_PATH = "src/test/unit/";

    static {
        final String implementation = capitalize(IMPLEMENTATION_CONFIGURATION_NAME);
        FUNCTIONAL_TEST_IMPLEMENTATION = FUNCTIONAL_TEST + implementation;
        INTEGRATION_TEST_IMPLEMENTATION = INTEGRATION_TEST + implementation;
    }

    @Override
    public void apply(@NotNull Project project) {
        final var plugins = project.getPlugins();
        plugins.apply(JavaPlugin.class);

        final var sourceSets = project.getExtensions().getByType(JavaPluginExtension.class).getSourceSets();

        sourceSets.getByName(Constants.TEST, unit -> {
            unit.java(dir -> dir.srcDirs(UNIT_PATH + Constants.JAVA));
            unit.resources(dir -> dir.srcDirs(UNIT_PATH + Constants.RESOURCES));
        });

        sourceSets.register(INTEGRATION_TEST, integration -> {
            integration.java(dir -> dir.srcDirs(INTEGRATION_PATH + Constants.JAVA));
            integration.resources(dir -> dir.srcDirs(INTEGRATION_PATH + Constants.RESOURCES));
        });

        sourceSets.register(FUNCTIONAL_TEST, functional -> {
            functional.java(dir -> dir.srcDirs(FUNCTIONAL_PATH + Constants.JAVA));
            functional.resources(dir -> dir.srcDirs(FUNCTIONAL_PATH + Constants.RESOURCES));
        });

        plugins.withType(GroovyPlugin.class, groovyPlugin -> {
            sourceSets.named(Constants.TEST, functional ->
                functional.getExtensions().getByType(GroovySourceDirectorySet.class)
                    .srcDirs(UNIT_PATH + Constants.GROOVY)
            );
            sourceSets.named(INTEGRATION_TEST, functional ->
                functional.getExtensions().getByType(GroovySourceDirectorySet.class)
                    .srcDirs(INTEGRATION_PATH + Constants.GROOVY)
            );
            sourceSets.named(FUNCTIONAL_TEST, functional ->
                functional.getExtensions().getByType(GroovySourceDirectorySet.class)
                    .srcDirs(FUNCTIONAL_PATH + Constants.GROOVY)
            );
        });

        final var dependencies = project.getDependencies();
        dependencies.add(INTEGRATION_TEST_IMPLEMENTATION, project);
        dependencies.add(FUNCTIONAL_TEST_IMPLEMENTATION, project);
    }
}
