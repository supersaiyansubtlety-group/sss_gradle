package net.sssubtlety.sss_gradle;

public interface Constants {
    String SSS_LIBS = "sssLibs";
    String JAVA = "java";
    String GROOVY = "groovy";
    String RESOURCES = "resources";
    String TEST = "test";
}
