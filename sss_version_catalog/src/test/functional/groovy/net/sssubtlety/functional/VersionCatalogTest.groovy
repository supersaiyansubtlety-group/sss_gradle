package net.sssubtlety.functional

import org.gradle.api.Project
import org.gradle.testkit.runner.GradleRunner
import spock.lang.Specification
import spock.lang.TempDir

import java.nio.file.Paths

import static org.gradle.api.Project.*
import static org.gradle.api.initialization.Settings.DEFAULT_SETTINGS_FILE

class VersionCatalogTest extends Specification {

    @TempDir File projectDir
    File gradleProperties
    File settingsGradle
    File buildGradle

    def setup() {
        final var generatedToml =
            Paths.get(DEFAULT_BUILD_DIR_NAME, 'version-catalog', 'libs.versions.toml')
                .toAbsolutePath().toString()
                .replace('\\' as char, '/' as char)

        gradleProperties = new File(projectDir, GRADLE_PROPERTIES)
        gradleProperties.createNewFile()

        buildGradle = new File(projectDir, DEFAULT_BUILD_FILE)
        buildGradle <<
            """
            plugins {
                id 'java'
            }
            repositories {
                mavenCentral()
            }
            """

        settingsGradle = new File(projectDir, DEFAULT_SETTINGS_FILE)
        settingsGradle <<
            """
            pluginManagement {
                repositories {
                    gradlePluginPortal()
                    mavenCentral()
                }
            }
            dependencyResolutionManagement {
                versionCatalogs {
                    sssLibs {
                        from(files("$generatedToml"))
                    }
                }
            }
            """
    }

    def "check catalog"() {
        when:
        buildGradle <<
            """
            dependencies {
                implementation $lib
            }
            """
        GradleRunner.create().withProjectDir(projectDir).build()

        then:
        noExceptionThrown()

        where:
        _| lib
        _| "platform(sssLibs.spock.bom)"
        _| "sssLibs.spock.core"
        _| "sssLibs.junit"
    }
}
